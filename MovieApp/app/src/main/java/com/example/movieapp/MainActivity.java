package com.example.movieapp;

import androidx.appcompat.app.AppCompatActivity;
//import android.support.v7.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import androidx.appcompat.widget.SearchView;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.widget.Toolbar;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class MainActivity extends AppCompatActivity {

    private Toolbar toolbar;
    ListView castListView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // toolbar display at app
        toolbar = findViewById(R.id.myToolBar);
        setSupportActionBar(toolbar);

         // assign values to each control in layout
        {
            TextView movieName = (TextView) findViewById(R.id.movieName);
            TextView yearTextView = (TextView) findViewById(R.id.yearTextView);
            TextView ratingsTextView = (TextView) findViewById(R.id.ratingsTextView);
            TextView votesTextView = (TextView) findViewById(R.id.votesTextView);
            TextView lengthTextView = (TextView) findViewById(R.id.lengthTextView);
            TextView plotTextView = (TextView) findViewById(R.id.plotTextView);
            ImageView posterImageView = (ImageView) findViewById(R.id.posterImageView);

            String url = "https://imdb-internet-movie-database-unofficial.p.rapidapi.com/film/inception";

            // show progress dialog bar before data populates from api
            ProgressDialog dialog=new ProgressDialog(MainActivity.this);
            dialog.setMessage("Data Loading");
            dialog.setCancelable(false);
            dialog.setInverseBackgroundForced(false);
            dialog.show();

            // Request a string response from the provided URL.
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                    (Request.Method.GET,
                            url,
                            null,
                            new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    try {

                                        String title = response.getString("title");
                                        String year = response.getString("year");
                                        String rating = response.getString("rating");
                                        String votes = response.getString("rating_votes");
                                        String length = response.getString("length");
                                        String plot = response.getString("plot");
                                        String poster = response.getString("poster");

                                   // set values to controls
                                        movieName.setText(title);
                                        yearTextView.setText(year);
                                        ratingsTextView.setText(rating);
                                        votesTextView.setText(votes);
                                        lengthTextView.setText(length);
                                        plotTextView.setText(plot);
                                        Glide.with(getApplicationContext())
                                                .load(poster)
                                                .into(posterImageView);

                                    // hide dialog bar
                                        dialog.hide();

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            System.out.println("error : " + error);
                            Toast.makeText(MainActivity.this, "Title not found", Toast.LENGTH_SHORT);
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("X-RapidAPI-Key", "5b6c1e071emsh837421049970682p1d23e6jsn03d92e694888");
                    params.put("X-RapidAPI-Host", "imdb-internet-movie-database-unofficial.p.rapidapi.com");
                    return params;
                }
            };
            // Add the request to the RequestQueue.
            MySingleton.getInstance(MainActivity.this).addToRequestQueue(jsonObjectRequest);

        }
}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);

        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setQueryHint("Search Movie By Name");

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                TextView movieName = (TextView) findViewById(R.id.movieName);
                TextView yearTextView = (TextView) findViewById(R.id.yearTextView);
                TextView ratingsTextView = (TextView) findViewById(R.id.ratingsTextView);
                TextView votesTextView = (TextView) findViewById(R.id.votesTextView);
                TextView lengthTextView = (TextView) findViewById(R.id.lengthTextView);
                TextView plotTextView = (TextView) findViewById(R.id.plotTextView);
                ImageView posterImageView = (ImageView) findViewById(R.id.posterImageView);

                String url ="https://imdb-internet-movie-database-unofficial.p.rapidapi.com/film/"+ searchView.getQuery().toString();

                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                        (Request.Method.GET,
                                url,
                                null,
                                new Response.Listener<JSONObject>() {
                                    @Override
                                    public void onResponse(JSONObject response) {
                                        try {
                                            String title = response.getString("title");
                                            String year = response.getString("year");
                                            String rating = response.getString("rating");
                                            String votes = response.getString("rating_votes");
                                            String length = response.getString("length");
                                            String plot = response.getString("plot");
                                            String poster = response.getString("poster");

                                            movieName.setText(title);
                                            yearTextView.setText(year);
                                            ratingsTextView.setText(rating);
                                            votesTextView.setText(votes);
                                            lengthTextView.setText(length);
                                            plotTextView.setText(plot);
                                            Glide.with(MainActivity.this)
                                                    .load(poster)
                                                    .into(posterImageView);

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                System.out.println("error : "+ error);
                                Toast.makeText(MainActivity.this, "Title not found", Toast.LENGTH_SHORT);
                                //movieName.setText("Search Movie with correct name! Existing data might show!");
                            }
                        }){

                          @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String>  params = new HashMap<String, String>();
                        params.put("X-RapidAPI-Key", "5b6c1e071emsh837421049970682p1d23e6jsn03d92e694888");
                        params.put("X-RapidAPI-Host", "imdb-internet-movie-database-unofficial.p.rapidapi.com");
                        return params;
                    }
                };
                // Add the request to the RequestQueue.
                MySingleton.getInstance(MainActivity.this).addToRequestQueue(jsonObjectRequest);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                TextView movieName = (TextView) findViewById(R.id.movieName);
                TextView yearTextView = (TextView) findViewById(R.id.yearTextView);
                TextView ratingsTextView = (TextView) findViewById(R.id.ratingsTextView);
                TextView votesTextView = (TextView) findViewById(R.id.votesTextView);
                TextView lengthTextView = (TextView) findViewById(R.id.lengthTextView);
                TextView plotTextView = (TextView) findViewById(R.id.plotTextView);
                ImageView posterImageView = (ImageView) findViewById(R.id.posterImageView);

                String url ="https://imdb-internet-movie-database-unofficial.p.rapidapi.com/film/"+ searchView.getQuery().toString();

                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                        (Request.Method.GET,
                                url,
                                null,
                                new Response.Listener<JSONObject>() {
                                    @Override
                                    public void onResponse(JSONObject response) {

                                        try {

                                            String title = response.getString("title");
                                            String year = response.getString("year");
                                            String rating = response.getString("rating");
                                            String votes = response.getString("rating_votes");
                                            String length = response.getString("length");
                                            String plot = response.getString("plot");
                                            String poster = response.getString("poster");


                                            movieName.setText(title);
                                            yearTextView.setText(year);
                                            ratingsTextView.setText(rating);
                                            votesTextView.setText(votes);
                                            lengthTextView.setText(length);
                                            plotTextView.setText(plot);

                                            Glide.with(MainActivity.this)
                                                    .load(poster)
                                                    .into(posterImageView);

                                        } catch (JSONException e) {

                                            e.printStackTrace();
                                        }

                                    }
                                }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                                Toast.makeText(MainActivity.this, "Title not found", Toast.LENGTH_SHORT);
                                //movieName.setText("Search Movie with correct name! Existing data might show!");
                            }
                        }){
                         @Override
                         public Map<String, String> getHeaders() throws AuthFailureError {
                         Map<String, String>  params = new HashMap<String, String>();
                         params.put("X-RapidAPI-Key", "5b6c1e071emsh837421049970682p1d23e6jsn03d92e694888");
                         params.put("X-RapidAPI-Host", "imdb-internet-movie-database-unofficial.p.rapidapi.com");
                         return params;
                }
            };

                // Add the request to the RequestQueue.
                MySingleton.getInstance(MainActivity.this).addToRequestQueue(jsonObjectRequest);
                return true;
            }
        });

        return true;

        }
    }


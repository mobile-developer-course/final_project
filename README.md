# README #

This readme document is explaining project work done for this course.

### What is this repository for? ###

* This repository contains the Android mobile developer final project by Zahir Babur.

### How do I get set up? ###

* Install Android studio.
* Install Java
* Install SDK

### Excerises work ###

* In exercises following functionality has been implemented.
  * Search movie app
  * App data is fetched from the free testing api https://rapidapi.com/hmerritt/api/imdb-internet-movie-database-unofficial/
  * App starts with spash screen
  * App contains topbar containing search view and home navigation button
  * Used singelton class for request que
  * App loads movie info data when started
  * Search view is implemented to search movie
  * Movie name, year, length, ratings, votes and plot description is loaded
  * Next thing is to show cast by using listview (which couldn't be done because lack of time before course deadline)


### Screenshots ###
   
   [![Screenshot-20210711-081223.png](https://i.postimg.cc/3xZNC8k1/Screenshot-20210711-081223.png)](https://postimg.cc/BjXJJGcP)
   [![Screenshot-20210711-081044.png](https://i.postimg.cc/cJZ1rVT3/Screenshot-20210711-081044.png)](https://postimg.cc/yDQCGpM6)
   [![Screenshot-20210711-081055.png](https://i.postimg.cc/DZp7pmhQ/Screenshot-20210711-081055.png)](https://postimg.cc/vg9RcYcD)
   [![Screenshot-20210711-082129.png](https://i.postimg.cc/dQ8YTWkm/Screenshot-20210711-082129.png)](https://postimg.cc/7bYcFnNf)